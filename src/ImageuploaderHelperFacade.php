<?php

namespace Noonic\Imageuploader;

use Illuminate\Support\Facades\Facade;

class ImageuploaderHelperFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'imageuploader';
    }
}