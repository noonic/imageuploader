<?php

namespace Noonic\Imageuploader;

use Illuminate\Support\ServiceProvider;
use Illuminate\Routing\Router;

class ImageuploaderServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('imageuploader', function() {
            return new ImageuploaderHelper;
        });
    }

    public function boot()
    {
        // Routes
        $this->setupRoutes($this->app->router);

          
        // Views
        $this->loadViewsFrom(__DIR__ . '/views', 'imageuploader');

//        // Publish
//        $this->publishes([
//            __DIR__ . '/config' => config_path('imageuploader'),
//            __DIR__ . '/views' => base_path('resources/views/vendor/imageuploader'),
//            __DIR__ . '/public' => base_path('public'),
//        ]);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function setupRoutes(Router $router)
    {
        $router->group(['namespace' => 'Noonic\Imageuploader\Http\Controllers'], function($router)
        {
            require __DIR__.'/Http/routes.php';
        });
    }
}