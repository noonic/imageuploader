<?php

namespace Noonic\Imageuploader\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Noonic\Imageuploader\ImageuploaderHelper;

class ImagesCropController extends Controller {
    protected $rules = [
        'image'  => [ 'required', 'mimes:jpeg,png,gif,bmp' ],
        'target' => [ 'required' ],
        'folder' => [ 'required' ]
    ];

    protected $uploadsDirectory;


    protected function _generateName( $filename, $sizeKey ) {
        return ImageuploaderHelper::getImage( $filename, $sizeKey );
    }

    protected function _checkOrCreateDirectory( $directory ) {
        if ( ! file_exists( $directory ) ) {
            if ( false === mkdir( $directory, 0777, true ) ) {
                return false;
            }
        }

        return $directory;
    }

    protected function _getSizes( $ratio ) {
        return isset( $this->config['sizes'][ $ratio ] ) ? $this->config['sizes'][ $ratio ] : null;
    }


    public function __construct() {
        $this->config               = Config::get( 'imageuploader' );
        $this->fullUploadsDirectory = base_path() . '/public/' . $this->config['uploads_directory'];
        $this->uploadsDirectory     = $this->config['uploads_directory'];
    }

    public function loading() {
        return view( 'imageuploader::loading' );
    }

    public function uploader( $target = 'image', $id = 'image', $folder = 'default', $skipcrop = false ) {
        $folder = base64_decode( $folder );
        
        return view( 'imageuploader::uploader', compact( 'target', 'id', 'folder', 'skipcrop' ) );
    }

    public function uploadImage( Request $request ) {
        $error = 'Invalid file.';

        $this->validate( $request, $this->rules );

        $input = Input::all();

        if ( $input['image']->isValid() ) {
            $sizes = $this->_getSizes( $input['ratio'] );

            $fileName = sha1( time() . uniqid() ) . '.' . $input['image']->getClientOriginalExtension(); // renaming image

            $saveDirectory = '/' . $input['folder'] . '/' . substr( $fileName, 0, 1 );

            $fullDirectory = $this->_checkOrCreateDirectory( $this->fullUploadsDirectory . $saveDirectory );
            $filePath      = $fullDirectory . '/' . $fileName;
            $imageUrl      = '/' . $this->uploadsDirectory . $saveDirectory . '/' . $fileName;


            $input['image']->move( $fullDirectory . '/', $fileName ); // uploading file to given path
            // sending back with message

            Session::flash( 'success', 'Immagine caricata con successo' );

            $redirectData = [
                'target'    => $input['target'],
                'id'        => $input['id'],
                'directory' => base64_encode( $saveDirectory ),
                'image'     => base64_encode( $fileName ),
                'url'       => base64_encode( $imageUrl ),
                'folder'    => base64_encode( $input['folder'] )
            ];

            if ( isset( $input['skipcrop'] ) && ( $input['skipcrop'] == 'true' || $input['skipcrop'] === '1' || $input['skipcrop'] === 1 ) ) {


                foreach ( $sizes as $sizeKey => $size ) {

                    $destinationFile = $this->_generateName( $filePath, $sizeKey );
                    Image::make( $filePath )->resize( $size[0], $size[1], function ( $constraint ) {
                        $constraint->aspectRatio();
                        $constraint->upsize();
                    } )->save( $destinationFile, $this->config['quality'] )->destroy();

                }
                $redirectData['url'] = base64_encode( $this->_generateName( $imageUrl, ! empty( $input['preview_size'] ) ? $input['preview_size'] : 't' ) );

                return redirect()->route( 'image_done', $redirectData );
            } else {
                return redirect()->route( 'image_cropper', $redirectData );
            }

        }

        return redirect()->route( 'image_uploader', [
            'target'   => $input['target'],
            'id'       => $input['id'],
            'folder'   => $input['folder'],
            'skipcrop' => $input['skipcrop']
        ] )->withErrors( [ 'error' => $error ] );
    }

    public function cropper( $target, $id, $directory, $image, $folder ) {
        if ( $target != '' && $directory != '' && $image != '' ) {
            $directory = base64_decode( $directory );
            $image     = base64_decode( $image );
            $folder    = base64_decode( $folder );

            return view( 'imageuploader::cropper', compact( 'target', 'id', 'directory', 'image', 'folder' ) );
        }

        return redirect()->route( 'image_uploader', $target );
    }

    public function crop() {
        $input = Input::all();

        $sizes = $this->_getSizes( $input['ratio'] );

        $saveDirectory = $input['directory'];

        $fullDirectory = $this->_checkOrCreateDirectory( $this->fullUploadsDirectory . $saveDirectory );
        $filePath      = $fullDirectory . '/' . $input['image'];


        foreach ( $sizes as $sizeKey => $size ) {

            $destinationFile = $this->_generateName( $filePath, $sizeKey );

            //var_dump( $filePath );
            Image::make( $filePath )->crop( intval( $input['w'] ), intval( $input['h'] ), intval( $input['x'] ), intval( $input['y'] ) )->resize( $size[0], $size[1], function ( $constraint ) {
                $constraint->aspectRatio();
                $constraint->upsize();
            } )->save( $destinationFile, $this->config['quality'] )->destroy();


        }

        $previewSize = ! empty( $input['preview_size'] ) ? $input['preview_size'] : 't';
        $imageUrl    = asset( $this->uploadsDirectory . $saveDirectory . '/' . $this->_generateName( $input['image'], $previewSize ) );

        return redirect()->route( 'image_done', [
            'target'    => $input['target'],
            'id'        => $input['id'],
            'directory' => base64_encode( $input['directory'] ),
            'image'     => base64_encode( $input['image'] ),
            'url'       => base64_encode( $imageUrl )
        ] );
    }


    public function getInput() {
        $input = Input::all();

        if ( isset( $input['name'] ) && isset( $input['folder'] ) ) {
            return view( 'imageuploader::_input', [
                'name'     => $input['name'],
                'id'       => isset( $input['id'] ) ? $input['id'] : $input['name'],
                'folder'   => $input['folder'],
                'data'     => isset( $input['data'] ) ? $input['data'] : '',
                'ratio'    => isset( $input['ratio'] ) ? $input['ratio'] : '4/3',
                'skipcrop' => isset( $input['skipcrop'] ) ? $input['skipcrop'] : false,
                'gallery'  => isset( $input['gallery'] ) ? $input['gallery'] : false
            ] );
        } else {
            return "Missing input data";
        }
    }


    public function done( $target, $id, $directory, $image, $url ) {
        if ( $target != '' && $directory != '' && $image != '' ) {
            $directory = base64_decode( $directory );
            $image     = base64_decode( $image );
            $url       = base64_decode( $url );

            return view( 'imageuploader::done', compact( 'target', 'id', 'directory', 'image', 'url' ) );
        }

        return redirect()->route( 'image_uploader', $target );
    }
}