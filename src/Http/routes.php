<?php

// Image
Route::group(['prefix' => '_image'], function() {
    // loading
    Route::get('/loading', [
        'as' => 'image_loading',
        'uses' => 'ImagesCropController@loading'
    ]);
    // uploader
    Route::get('/uploader/{target}/{id}/{folder}/{skipCrop?}', [
        'as' => 'image_uploader',
        'uses' => 'ImagesCropController@uploader'
    ]);
    // upload image
    Route::post('/upload', [
        'as' => 'image_upload_image',
        'uses' => 'ImagesCropController@uploadImage'
    ]);
    // cropper
    Route::get('/cropper/{target}/{id}/{directory}/{image}/{folder}', [
        'as' => 'image_cropper',
        'uses' => 'ImagesCropController@cropper'
    ]);
    // crop
    Route::post('/crop', [
        'as' => 'image_crop',
        'uses' => 'ImagesCropController@crop'
    ]);
    // done
    Route::get('/done/{target}/{id}/{directory}/{image}/{url}', [
        'as' => 'image_done',
        'uses' => 'ImagesCropController@done'
    ]);
    // get input
    Route::post('/get-input', [
        'as' => 'image_get_input',
        'uses' => 'ImagesCropController@getInput'
    ]);
});